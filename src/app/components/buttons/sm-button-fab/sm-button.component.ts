import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sm-button',
  templateUrl: './sm-button.component.html',
  styleUrls: ['./sm-button.component.scss'],
})
export class SmButtonComponent implements OnInit {
  @Input()
  disabled: boolean;
  @Input()
  type: string;

  @Input()
  position: string;

  @Input()
  icon: string;

  /*@Output()
  onClick*/
  constructor() {
  }

  ngOnInit() {
  }

}
