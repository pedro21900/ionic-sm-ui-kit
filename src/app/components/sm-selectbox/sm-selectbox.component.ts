import {Component, forwardRef, Input, OnInit, Provider} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {SmMapperObjectService} from "../services/sm-mapper-object.service";
import {SmDefaultObject} from "../object/sm-default-object";


const COM_PREFERENCE_CONTROL_VALUE_ACCESSOR: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SmSelectboxComponent),
  multi: true,
};


@Component({
  selector: 'sm-selectbox',
  templateUrl: './sm-selectbox.component.html',
  styleUrls: ['./sm-selectbox.component.scss'],
  providers: [COM_PREFERENCE_CONTROL_VALUE_ACCESSOR]
})
export class SmSelectboxComponent implements OnInit, ControlValueAccessor {
  @Input()
  name: string;
  @Input()
  type: string;
  @Input()
  placeholder: string;
  @Input()
  datasource: any;
  @Input()
  icon: string;
  @Input()
  title: string;
  @Input()
  valueExpr: string;
  @Input()
  displayExpr: string;

  value: string;
  private onTouched!: Function;
  private onChanged!: Function;
  private smDefaultObjects: SmDefaultObject[] = [];

  constructor(private smMapperObjectService: SmMapperObjectService) {
  }

  objectMapper() {
    this.smDefaultObjects = this.smMapperObjectService
      .exprValue(this.datasource, this.valueExpr, this.displayExpr)
  }

  handleChange(event: any) {
    this.objectMapper();
    this.onChanged(event.detail.value);
  }

  focusClick(event: any) {
    this.onTouched()
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChanged = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnInit(): void {
    console.log(this.datasource)
    this.objectMapper();
  }

}
