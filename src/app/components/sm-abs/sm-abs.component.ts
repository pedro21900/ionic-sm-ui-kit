import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sm-abs',
  templateUrl: './sm-abs.component.html',
  styleUrls: ['./sm-abs.component.scss'],
})
export class SmAbsComponent implements OnInit {

  @Input()
  tab: any[];

  @Input()
  primary: string;

  constructor() {
  }

  ngOnInit() {
  }

}
