import {NgModule} from '@angular/core';
import {SmCardComponent} from './sm-card.component';
import {IonicModule} from '@ionic/angular';
import {ReactiveFormsModule} from '@angular/forms';
import {MatExpansionModule} from '@angular/material/expansion';

@NgModule({
  declarations: [SmCardComponent],
  imports: [IonicModule, ReactiveFormsModule, MatExpansionModule],
  exports: [SmCardComponent],
})
export class SmCardModule {
}
