import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sm-card',
  templateUrl: './sm-card.component.html',
  styleUrls: ['./sm-card.component.scss'],
})
export class SmCardComponent implements OnInit {

  @Input()
  title:string;
  @Input()
  icon:string;
  @Input()
  color:string;

  constructor() { }

  ngOnInit() {}

}
