import {
  AfterContentChecked,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  OnInit,
  Provider,
  ViewChild
} from '@angular/core';
import {Subscription} from "rxjs";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {SmAccordionService} from "./service/sm-accordion.service";
import {IonToggle} from "@ionic/angular";

const COM_PREFERENCE_CONTROL_VALUE_ACCESSOR: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SmAccordionComponent),
  multi: true,
};

@Component({
  selector: 'sm-accordion',
  templateUrl: './sm-accordion.component.html',
  styleUrls: ['./sm-accordion.component.scss'],
  providers: [COM_PREFERENCE_CONTROL_VALUE_ACCESSOR],
})
export class SmAccordionComponent implements OnInit, ControlValueAccessor, AfterContentChecked {
  @Input() title: string;
  @Input() description: string;
  @Input() hideToggle: boolean;
  @ViewChild('teste') ionToggle: IonToggle;
  subscription: Subscription;
  valueInput: boolean = false;
  expanded: boolean;
  private onTouched!: Function;

  onChanged = (_) => {
  };

  constructor(private smAccordionService: SmAccordionService, private cdRef: ChangeDetectorRef) {
  }

  expandedEvent(expanded: boolean) {
    this.onChanged(expanded);
  }

  writeValue(value: boolean): void {
    this.valueInput = value;
  }

  registerOnChange(fn: (_: any) => void): void {
    this.onChanged = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnInit() {
    //   this.smAccordionService.emitterChecked
    //     .subscribe(emitterChecked =>{ this.valueInput = emitterChecked; console.log(emitterChecked)});
  }

  ngAfterContentChecked(): void {
    this.cdRef.detectChanges();
  }
}
