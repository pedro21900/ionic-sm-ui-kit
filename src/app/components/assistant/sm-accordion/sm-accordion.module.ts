
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SmAccordionComponent} from "./sm-accordion.component";
import {IonicModule} from "@ionic/angular";
import {ReactiveFormsModule} from "@angular/forms";
import {MatExpansionModule} from '@angular/material/expansion';
import {SmItemComponent} from "../../sm-item/sm-item.component";
import { SmBadgeComponent } from '../../output/sm-badge/sm-badge.component';

@NgModule({
  declarations: [SmAccordionComponent, SmItemComponent, SmBadgeComponent],
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    MatExpansionModule,
  ],
  exports: [SmAccordionComponent, SmItemComponent, SmBadgeComponent],
})
export class SmAccordionModule {}
