import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sm-parent-child',
  templateUrl: './sm-parent-child.component.html',
  styleUrls: ['./sm-parent-child.component.scss'],
  animations: [
    trigger('smoothCollapse', [
      state(
        'initial',
        style({
          height: '0',
          overflow: 'hidden',
          opacity: '0',
        })
      ),
      state(
        'final',
        style({
          margin: '0 20px 20px 20px',
        })
      ),
      transition('initial<=>final', animate('270ms ease-in-out')),
    ]),
    trigger('rotateIcon', [
      state(
        'default',
        style({
          transform: 'rotate(0deg)',
        })
      ),
      state(
        'rotated',
        style({
          transform: 'rotate(180deg)',
        })
      ),
      transition('default<=>rotated', animate('270ms ease-in-out')),
    ]),
    trigger('changeColor', [
      state(
        'white',
        style({
          background: '#fff',
          color: 'var(--ion-color-primary)',
        })
      ),
      state(
        'colored',
        style({
          background: 'var(--ion-color-primary)',
          color: '#fff',
        })
      ),
    ]),
  ],
})
export class SmParentChildComponent implements OnInit {
  showBody: boolean = false;

  constructor() {}

  ngOnInit() {}

  toggle() {
    this.showBody = !this.showBody;
  }
}
