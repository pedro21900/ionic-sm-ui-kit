import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmParentChildComponent } from './sm-parent-child.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [SmParentChildComponent],
  imports: [CommonModule],
  exports: [SmParentChildComponent],
})
export class SmParentChildModule {}
