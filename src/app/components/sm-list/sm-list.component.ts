import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sm-list',
  templateUrl: './sm-list.component.html',
  styleUrls: ['./sm-list.component.scss'],
})
export class SmListComponent implements OnInit {

  @Input()
  smItems: any[];

  constructor() {
  }

  ngOnInit() {
  }
}
