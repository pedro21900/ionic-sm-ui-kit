import {Component, Input, OnInit} from '@angular/core';
import {ActionSheetController} from "@ionic/angular";
import {Router} from "@angular/router";

@Component({
  selector: 'sm-item',
  templateUrl: './sm-item.component.html',
  styleUrls: ['./sm-item.component.scss'],
})
export class SmItemComponent implements OnInit {

  @Input() slot: string;
  @Input()
  smItem: any;

  constructor(
    private actionSheetController: ActionSheetController,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Opções',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Editar',
        role: 'destructive',
        icon: 'pencil',
        id: 'delete-button',
        data: {
          type: 'delete'
        },
        handler: () => {
          this.edit(this.smItem.id)
        }
      }, {
        text: 'Detalhes',
        icon: 'newspaper',
        data: 10,
        handler: () => {
          this.detail(this.smItem.id)
        }
      }, {
        text: 'Excluir',
        icon: 'trash-bin',
        data: {
          type: 'delete'
        },
        role: 'destructive',
        handler: () => {
          console.log(this.smItem.id);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
      }]
    });
    await actionSheet.present();

    const {role, data} = await actionSheet.onDidDismiss();

  }

  edit(id: number) {
    this.router.navigate(['/instalacao-armadilha', 'edit', id]);
  }

  detail(id: number) {
    this.router.navigate(['/instalacao-armadilha/detail/', id]);
  }

  /* delete(id: number) {
     this.router.navigate(['/instalacao-armadilha', 'edit', id]);
   }*/


}
