import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'instalacao-de-armadilha',
    pathMatch: 'full'
  },
  {
    path: 'instalacao-armadilha',
    loadChildren: () => import('./pages/instalacao-armadilha/instalacao-armadilha.module').then(m => m.InstalacaoArmadilhaPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
