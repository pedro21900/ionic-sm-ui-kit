import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-instalacao-armadilha',
  templateUrl: './instalacao-armadilha.page.html',
  styleUrls: ['./instalacao-armadilha.page.scss'],
})
export class InstalacaoArmadilhaPage implements OnInit {
  id: number;

  constructor(private title: Title,
              private router: Router, private activatedRoute: ActivatedRoute) {

    router.events.pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd | any) => {
        this.id = +this.activatedRoute.snapshot.params['idArmadilha'];
        switch (event.url) {
          case '/instalacao-armadilha/edit':
            title.setTitle('Cadastro de Armadilha');
            break;

          case this.findTerm(event.url, "/edit/"):
            title.setTitle('Atualização da Armadilha');
            break;

          case this.findTerm(event.url, "/instalacao-armadilha/detail/"):
            title.setTitle('Detalhes da Armadilha');
            break;

          case this.findTerm(event.url, "/instalacao-armadilha/mapa"):
            title.setTitle('Mapa de Armadilha');
            break;

          default:
            title.setTitle('Armadilha');
            break;
        }
      });

  }

  ngOnInit() {
  }

  findTerm(value: string, search: string) {
    if (value.includes(search)) {
      return value;
    }

  }
}
