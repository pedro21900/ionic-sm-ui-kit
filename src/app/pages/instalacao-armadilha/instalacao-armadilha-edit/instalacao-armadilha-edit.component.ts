import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NavController} from "@ionic/angular";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import {Title} from "@angular/platform-browser";
import {Observable, of} from "rxjs";
import {delay} from "rxjs/operators";

@Component({
  selector: 'app-instalacao-armadilha',
  templateUrl: './instalacao-armadilha-edit.component.html',
  styleUrls: ['./instalacao-armadilha-edit.component.scss'],
})
export class InstalacaoArmadilhaEditComponent implements OnInit {

  id: number;



  hospedeiro$: Observable<{ id: string; value: string; }[]>;

  tipoArmadilha: Object[] = [{name: 'Jackson', value: 'J'}, {name: 'McPhail', value: 'M'}];

  estado: string[] = ['Maça', 'Perâ', 'Laranja'];

  /**
   * Variável de instância do FormGroup;
   * @private
   */
  private todo: FormGroup;

  constructor(private router: Router,
              private navCtrl: NavController,
              private activatedRoute: ActivatedRoute,
              private formBuilder: FormBuilder,
              private title: Title
  ) {


    this.hospedeiro$ = of([{id: '1', value: 'Maça'},
      {id: '2', value: 'Laranja'},
      {id: '3', value: 'Perâ'}])
      .pipe(delay(1000));

    this.todo = this.formBuilder.group({
      hospedeiro: ['', Validators.required],
      estado: ['', Validators.required],
      tipoArmadilha: ['J', Validators.required],

    });
  }

  ngOnInit() {
    this.id = +this.activatedRoute.snapshot.params['idArmadilha'];

    if (this.id && !isNaN(this.id)) {
      this.title.setTitle("Edição Armadilha");
      this.edit();
    } else {
      /*  this.breadcrumbService.set('pessoa/edit', 'Novo pessoa');*/
      this.create();
    }
  }


  back() {
    this.navCtrl.back();
  }

  save(valid: boolean) {
    if (valid) {
      console.log(this.todo.value)
      this.back()

    } else {

    }

  }

  create() {

  }

  edit() {
    /*this.title.setTitle('Edição de Pessoa');*/
    /*  this.pessoaService
        .findById(this.id)
        .pipe(take(1))
        .subscribe(pessoa => (this.pessoaFormData = pessoa));*/
    /*preencher form com objeto*/

  }

  /*  toFormGroup(questions: QuestionBase<string>[]) {
      const group: any = {};

      questions.forEach(question => {
        group[question.key] = question.required ? new FormControl(question.value || '', Validators.required)
          : new FormControl(question.value || '');
      });
      return new FormGroup(group);
    }*/
}
