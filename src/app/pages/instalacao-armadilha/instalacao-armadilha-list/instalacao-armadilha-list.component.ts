import {AfterViewInit, Component} from '@angular/core';
import {Router} from '@angular/router';
import {CulturaService} from "../../../service/cultura.service";

@Component({
  selector: 'app-instalacao-armadilha',
  templateUrl: './instalacao-armadilha-list.component.html',
  styleUrls: ['./instalacao-armadilha-list.component.scss'],
})
export class InstalacaoArmadilhaListComponent implements AfterViewInit {
  panelOpenState: boolean;
  panelOpenState2: boolean;

  tab: any[] = [
    {
      title: 'ARMADILHAS',
      icon: 'bug',
      routerlink: '/instalacao-armadilha/armadilha',
    },
    {title: 'MAPA', icon: 'map', routerlink: '/instalacao-armadilha/mapa'},
  ];

  traps: Trap[] = [
    {
      date: '10/02/2021',
      type: 'Jackson',
      lat: '41°24\'12.2"N 2°10\'26.5"',
      long: '65°24\'78.2"N 2°42\'56.5"',
      prague: 'mosca-branca',
    },
    {
      date: '16/02/2021',
      type: 'Jackson',
      lat: '41°24\'12.2"N 2°10\'26.5"',
      long: '65°24\'78.2"N 2°42\'56.5"',
      prague: 'mosca-branca',
    },
    {
      date: '16/02/2021',
      type: 'Jackson',
      lat: '41°24\'12.2"N 2°10\'26.5"',
      long: '65°24\'78.2"N 2°42\'56.5"',
      prague: 'mosca-branca',
    },
    {
      date: '16/02/2021',
      type: 'Jackson',
      lat: '41°24\'12.2"N 2°10\'26.5"',
      long: '65°24\'78.2"N 2°42\'56.5"',
      prague: 'mosca-branca',
    },
    {
      date: '16/02/2021',
      type: 'JacksonpanelOpenState',
      lat: '41°24\'12.2"N 2°10\'26.5"',
      long: '65°24\'78.2"N 2°42\'56.5"',
      prague: 'mosca-branca',
    },
    {
      date: '16/02/2021',
      type: 'Jackson',
      lat: '41°24\'12.2"N 2°10\'26.5"',
      long: '65°24\'78.2"N 2°42\'56.5"',
      prague: 'mosca-branca',
    },
    {
      date: '16/02/2021',
      type: 'Jackson',
      lat: '41°24\'12.2"N 2°10\'26.5"',
      long: '65°24\'78.2"N 2°42\'56.5"',
      prague: 'mosca-branca',
    },
    {
      date: '16/02/2021',
      type: 'Jackson',
      lat: '41°24\'12.2"N 2°10\'26.5"',
      long: '65°24\'78.2"N 2°42\'56.5"',
      prague: 'mosca-branca',
    },
  ];

  constructor(private router: Router, private culturaService: CulturaService) {
    this.culturaService.databaseConn();
  }

  ngAfterViewInit(): void {
    // this.culturaService.insert('Amazônia',
    //   'João Batista',
    //   'Lat: N 35º 12º 345',
    //   'Lat: N 35º 12º 345',
    //   1)
    console.log(this.culturaService.findAll());
  }

  ngOnInit() {

  }

  create() {
    this.router.navigate(['/instalacao-armadilha', 'edit']);
  }

  /**
   *
   * @param id
   */
  edit(id: number) {
    this.router.navigate(['/instalacao-armadilha', 'edit', id]);
  }

  toggleButtonState(buttonState: boolean) {
    // this.panelOpenState = buttonState;
  }
}

interface Trap {
  type: string;
  date: string;
  prague: string;
  lat: string;
  long: string;
}
