import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {InstalacaoArmadilhaPage} from './instalacao-armadilha.page';
import {InstalacaoArmadilhaListComponent} from './instalacao-armadilha-list/instalacao-armadilha-list.component';
import {InstalacaoArmadilhaEditComponent} from './instalacao-armadilha-edit/instalacao-armadilha-edit.component';

const routes: Routes = [
  {
    path: '',
    component: InstalacaoArmadilhaPage, children: [
      {
        path: '', component: InstalacaoArmadilhaListComponent, children: [
          // {path: 'armadilha', component: ArmadilhaComponent},
          // {path: 'mapa', component: MapaComponent}
        ]
      },
      {
        path: 'edit', children: [
          {path: '', component: InstalacaoArmadilhaEditComponent},
          {path: ':idArmadilha', component: InstalacaoArmadilhaEditComponent}
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InstalacaoArmadilhaPageRoutingModule {
}
