import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InstalacaoArmadilhaPageRoutingModule } from './instalacao-armadilha-routing.module';

import { InstalacaoArmadilhaPage } from './instalacao-armadilha.page';
import { InstalacaoArmadilhaListComponent } from './instalacao-armadilha-list/instalacao-armadilha-list.component';
import { InstalacaoArmadilhaEditComponent } from './instalacao-armadilha-edit/instalacao-armadilha-edit.component';
import { SmCardComponent } from '../../components/sm-card/sm-card.component';
import { SmButtonComponent } from '../../components/buttons/sm-button-fab/sm-button.component';
import { SmAbsComponent } from '../../components/sm-abs/sm-abs.component';
import { SmListComponent } from '../../components/sm-list/sm-list.component';
import { SmAccordionModule } from '../../components/assistant/sm-accordion/sm-accordion.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SmParentChildModule } from 'src/app/components/assistant/sm-parent-child/sm-parent-child.module';

@NgModule({
  declarations: [
    InstalacaoArmadilhaPage,
    InstalacaoArmadilhaListComponent,
    InstalacaoArmadilhaEditComponent,

    SmCardComponent,
    SmButtonComponent,
    SmAbsComponent,
    SmListComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    InstalacaoArmadilhaPageRoutingModule,
    SmAccordionModule,
    SmParentChildModule,
    IonicModule,
    MatSlideToggleModule,
  ],
})
export class InstalacaoArmadilhaPageModule {}
