# Executar projeto

## Pré-requisitos

* Java 8
* Node 12.20.3
* Angular CLI 

### Passo 1 - Build do projeto

```shell
    npx ionic cordova build android
```

### Passo 2 - Executar projeto no dispositivo

```shell
    native-run android --app platforms/android/app/build/outputs/apk/debug/app-debug.apk --device
```

### Compilar e executa projeto em tempo real no dipositivo.

```shell
    npx ionic cordova run android -l
```
